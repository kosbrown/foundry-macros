// On Use Macros: Before Damange Application
const caster = canvas.tokens.get(args[0].tokenId);

const effect = "jb2a.breath_weapons.poison.cone.green";

await Sequencer.Preloader.preloadForClients([effect])


for (const target of args[0].targets) {
    new Sequence()
        .effect()
            .file(effect)
            .atLocation(caster)
            .stretchTo(target)
    .play();
}

// Wait until part of the animation is finished before processing the damage
await new Promise(r => setTimeout(r, 1200));
