const preExplosionEffect = "jb2a.fireball.beam.orange";
const explosionEffect = "jb2a.fireball.explosion.orange"
const postExplosionEffect = "jb2a.ground_cracks.orange.0" + (Math.floor(Math.random() * 2) +1);

for (let i = 0; i< 5; i++) {
    const caster = getRandomPointAround(game.canvas.dimensions.width/2, game.canvas.dimensions.height/2, 12);
    const position = getRandomPointAround(game.canvas.dimensions.width/2, game.canvas.dimensions.width/2, (Math.random() * 9)+1);
    caseFireball(caster, position);
    console.log("Fireball #" +i);
    await new Promise(r => setTimeout(r, (Math.random()*500)+1200));
}

function caseFireball(from, to) {
    const postExplosionEffect = "jb2a.ground_cracks.orange.0" + (Math.floor(Math.random() * 2) +1);
    new Sequence()
        .effect()
            .file(preExplosionEffect)
            .zIndex(1)
            .atLocation(from)
            .stretchTo(to)
        .wait(2200)
        .effect()
            .file(explosionEffect)
            .zIndex(2)
            .atLocation(to)
            .randomRotation()
            .scale(1.3)
            .scaleIn(0.2, 222)
        .wait(1500)
        .effect()
            .file(postExplosionEffect)
            .zIndex(1)
            .belowTokens()
            .randomRotation()
            .scale(1.3)
            .duration(6000)
            .fadeIn(500)
            .fadeOut(2000)            
        .atLocation(to)
    .play()
}

function getRandomPointAround(x, y, size) {
    const radius = size * canvas.dimensions.size;
    const angle = Math.random()*Math.PI*2;
    return {x: x + Math.cos(angle)*radius, y: y + Math.sin(angle)*radius }
}
