for (const token of canvas.tokens.placeables) {
    const caster = getRandomPointAround(game.canvas.dimensions.width/2, game.canvas.dimensions.height/2, 100);
    const preExplosionEffect = "jb2a.fireball.beam.orange";
    const explosionEffect = "jb2a.fireball.explosion.orange"
    const postExplosionEffect = "jb2a.ground_cracks.orange.0" + (Math.floor(Math.random() * 2) +1);
    const position = {x: token.center.x, y: token.center.y };
    new Sequence()
        .effect()
            .file(preExplosionEffect)
            .zIndex(1)
            .atLocation(caster)
            .stretchTo(position)
        .wait(2200)
        .effect()
            .file(explosionEffect)
            .zIndex(2)
            .atLocation(position)
            .randomRotation()
            .scale(1.3)
            .scaleIn(0.2, 222)
        .wait(1500)
        .effect()
            .file(postExplosionEffect)
            .zIndex(1)
            .belowTokens()
            .randomRotation()
            .scale(1.3)
            .duration(6000)
            .fadeIn(500)
            .fadeOut(2000)
        .atLocation(position)
    .play()
    await new Promise(r => setTimeout(r, (Math.random()*500)+2500));
}


function getRandomPointAround(x, y, size) {
    const radius = size * canvas.dimensions.size * 0.60;
    const angle = Math.random()*Math.PI*2;
    return {x: x + Math.cos(angle)*radius, y: y + Math.sin(angle)*radius }
}
