// On Use Macros: After targeting complete

const actorData = args[0].actor;
const targets = args[0].targets;

if (targets.length != 1) {
    ui.notifications.error("You must select exactly one target, no more, no less.");
    return false;
}

const target = targets[0];
const targetDC = 8 + actorData.data.prof.flat + actorData.data.abilities.wis.mod;

console.log(actorData);
console.log(target);

new Dialog({
    title: "Open Hand Technique",
    content: "<p>Choose an effect:</p>",
    buttons: {
        prone: {
            icon: '<i class="fas fa-person-harassing"></i>',
            label: "Knock prone",
            callback: async () => {
                ChatMessage.create({
                    content: "I CAST FIST!",
                    speaker: ChatMessage.getSpeaker(game.actors.get(actorData._id))
                }, { chatBubble: true });
                const roll = await target.actor.rollAbilitySave('dex', {
                    defaultRollMode: "publicroll",
                    flavor: "Attempting to resist being knocked prone by " + actorData.token.name  + ", rolling Dexterity save against DC " + targetDC,
                    targetValue: targetDC
                });
                await new Promise(r => setTimeout(r, 1500));
                if (targetDC > roll.total) {
                    await MidiQOL.socket().executeAsGM("createEffects", {
                        actorUuid: target.actor.uuid, effects: [
                            {
                                label: "Prone",
                                icon: "modules/combat-utility-belt/icons/prone.svg",
                                disabled: false,
                                duration: { rounds: 10, seconds: 60, startRound: game.combat ? game.combat.round : 0, startTime: game.time.worldTime },
                                changes: [
                                    { key: `flags.midi-qol.disadvantage.attack.all`, mode: 2, value: 1, priority: 20 },
                                    { key: `flags.midi-qol.grants.advantage.attack.mwak`, mode: 2, value: 1, priority: 20 },
                                    { key: `flags.midi-qol.grants.advantage.attack.msak`, mode: 2, value: 1, priority: 20 },
                                    { key: `flags.midi-qol.grants.disadvantage.attack.rwak`, mode: 2, value: 1, priority: 20 },
                                    { key: `flags.midi-qol.grants.disadvantage.attack.rsak`, mode: 2, value: 1, priority: 20 }
                                ]
                            }
                        ]
                    });
                    await ChatMessage.create({
                        content: target.name + " is knocked prone! You can't stand against my greatness!",
                        speaker: ChatMessage.getSpeaker(game.actors.get(actorData._id))
                    });
                } else {
                    await ChatMessage.create({
                        content: target.name + " is still standing. Somehow. Must be cheating.",
                        speaker: ChatMessage.getSpeaker(game.actors.get(actorData._id))
                    });
                }
            }
        },
        push: {
            icon: '<i class="fas fa-person-falling"></i>',
            label: "Push back",
            callback: async () => {
                ChatMessage.create({
                    content: "YEET!",
                    speaker: ChatMessage.getSpeaker(game.actors.get(actorData._id))
                }, { chatBubble: true });
                const roll = await target.actor.rollAbilitySave('str', {
                    defaultRollMode: "publicroll",
                    flavor: "Attempting to resist thrown around by " + actorData.token.name  + ", rolling Strength save against DC " + targetDC,
                    targetValue: targetDC
                });
                await new Promise(r => setTimeout(r, 1500));
                if (targetDC > roll.total) {
                    await ChatMessage.create({
                        content: "There you go " + target.name + ", up to 15ft away from me!",
                        speaker: ChatMessage.getSpeaker(game.actors.get(actorData._id))
                    });
                } else {
                    await ChatMessage.create({
                        content: target.name + " is a lot heavier than it looks.",
                        speaker: ChatMessage.getSpeaker(game.actors.get(actorData._id))
                    });
                }
            }
        },
        reaction: {
            icon: '<i class="fas fa-person-rays"></i>',
            label: "No reactions",
            callback: async () => {
                ChatMessage.create({
                    content: "COME AT ME BRO!",
                    speaker: ChatMessage.getSpeaker(game.actors.get(actorData._id))
                }, { chatBubble: true });
                await new Promise(r => setTimeout(r, 800));
                target.actor.rollAbilitySave('int', {
                    defaultRollMode: "publicroll",
                    flavor: "Can't take reactions until the end of " + actorData.token.name + "'s next turn.",
                    targetValue: 999
                });
            }
        },
    },
    default: "none"
}).render(true);

// Abort the default workflow
return false;
