// On Use Macros: After targeting complete
//  ... and also: After Attack Roll
//  ... and also: Before Damage Roll
//  ... and also: Before Damange Application
// Manualy set the Damage type to None in the Damage Formula section

const currentStage = args[0].macroPass;
const workflow = MidiQOL.Workflow.getWorkflow(args[0].uuid);

if (currentStage == "preambleComplete") {
    // Default settings
    workflow.overrideDamageType = "acid"
    workflow._macro_projectileEffect = "jb2a.dancing_light.green";
    workflow._macro_explosionEffect = "jb2a.toll_the_dead.green.shockwave";
    new Dialog({
        title: "Chromatic Orb",
        content: "<p>Choose damage type for chromatic orb:</p>",
        buttons: {
            acid: {
                icon: '<i class="fas fa-vial"></i>',
                label: "Acid",
                callback: () => {
                    workflow.overrideDamageType = 'acid'
                    workflow._macro_projectileEffect = "jb2a.dancing_light.green";
                    workflow._macro_explosionEffect = "jb2a.toll_the_dead.green.shockwave";
                }

            },
            cold: {
                icon: '<i class="fas fa-icicles"></i>',
                label: "Cold",
                callback: () => {
                    workflow.overrideDamageType = 'cold'
                    workflow._macro_projectileEffect = "jb2a.dancing_light.blueteal";
                    workflow._macro_explosionEffect = "jb2a.impact_themed.ice_shard.blue";
                }
            },
            fire: {
                icon: '<i class="fas fa-fire"></i>',
                label: "Fire",
                callback: () => {
                    workflow.overrideDamageType = 'fire'
                    workflow._macro_projectileEffect = "jb2a.dancing_light.yellow";
                    workflow._macro_explosionEffect = "jb2a.firework.0" +  + (Math.floor(Math.random() * 1) +1) + ".orangeyellow.0" + (Math.floor(Math.random() * 1) +1);
                }
            },
            lightning: {
                icon: '<i class="fas fa-bolt"></i>',
                label: "Lightning",
                callback: () => {
                    workflow.overrideDamageType = 'lightning'
                    workflow._macro_projectileEffect = "jb2a.lightning_ball.blue";
                    workflow._macro_explosionEffect = "jb2a.explosion.blue." + (Math.floor(Math.random() * 1) +1);
                }
            },
            poison: {
                icon: '<i class="fas fa-flask"></i>',
                label: "Poison",
                callback: () => {
                    workflow.overrideDamageType = 'poison'
                    workflow._macro_projectileEffect = "jb2a.dancing_light.purplegreen";
                    workflow._macro_explosionEffect = "jb2a.toll_the_dead.green.skull_smoke";
                }
            },
            thunder: {
                icon: '<i class="fas fa-cloud"></i>',
                label: "Thunder",
                callback: () => {
                    workflow.overrideDamageType = 'thunder'
                    workflow._macro_projectileEffect = "jb2a.lightning_ball.blue";
                    workflow._macro_explosionEffect = "jb2a.explosion.blue." + (Math.floor(Math.random() * 1) +1);
                }
            }
        },
        default: "acid"
    }).render(true);
}

if (currentStage == "postAttackRoll") {
    const casterToken = canvas.scene.data.tokens.find(token => token.actor.id == args[0].actor._id)
    workflow._macro_effects = [];
    for (const target of args[0].targets) {
        // Add spell effect
        const isHit = args[0].hitTargets?.filter(item => item.id == target.id).length > 0;
        const targetPosition = isHit ? target : getRandomPointAround(target.data.x, target.data.y, target.data.width * target.data.scale);
        const spellEffect = new Sequence()
            .effect()
                .file(workflow._macro_projectileEffect)
                .scale(0.4)
                .atLocation(casterToken)
                .moveTowards(targetPosition, {ease: "easeInCubic"})
                .moveSpeed(1000)
            .waitUntilFinished()
            .effect()
                .file(workflow._macro_explosionEffect)
                .scale(0.7)
                .atLocation(targetPosition)
        workflow._macro_effects.push(spellEffect);
    }
    if (workflow.isFumble) {
        // If the attack is a fumble, there will be no damage application setp, so play the animation now
        await Sequencer.Preloader.preloadForClients([workflow._macro_projectileEffect, workflow._macro_explosionEffect])
        workflow._macro_effects.forEach(effect => effect.play());
    }
}

if (currentStage == "preDamageRoll") {
    // Before damage roll, override the damage type with the chosen value
    workflow.defaultDamageType = workflow.overrideDamageType;
    await Sequencer.Preloader.preloadForClients([workflow._macro_projectileEffect, workflow._macro_explosionEffect])
}

if (currentStage == "preDamageApplication") {
    workflow._macro_effects.forEach(effect => effect.play());
    await new Promise(r => setTimeout(r, 1000));
}

function getRandomPointAround(x, y, size) {
    const radius = size * canvas.dimensions.size * 0.60;
    const angle = Math.random()*Math.PI*2;
    return {x: x + Math.cos(angle)*radius, y: y + Math.sin(angle)*radius }
}
